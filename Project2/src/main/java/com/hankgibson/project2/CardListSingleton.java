package com.hankgibson.project2;

import java.util.ArrayList;

/**
 * Created by hankgibson on 10/3/13.
 */
public class CardListSingleton
{
    private static CardListSingleton ourInstance = new CardListSingleton();
    private ArrayList<FlashCard> cardArrayList;

    //allows us to get the instance of our own class
    public static CardListSingleton getInstance()
    {
        if(ourInstance == null)
        {
            ourInstance = new CardListSingleton();
        }
        return ourInstance;
    }

    //create a new list if one is not created yet
    private CardListSingleton()
    {
        // Setup ArrayList
        cardArrayList = new ArrayList<FlashCard>();

        //flash card 1
        FlashCard flashcard1 = new FlashCard();
        flashcard1.setmQuestion("What is the capitol of Idaho?");
        flashcard1.setmAnswer("Boise");

        //flash card 2
        FlashCard flashcard2 = new FlashCard();
        flashcard2.setmQuestion("What is the capitol of California?");
        flashcard2.setmAnswer("Sacramento");

        //flash card 3
        FlashCard flashcard3 = new FlashCard();
        flashcard3.setmQuestion("What is the capitol of Kansas?");
        flashcard3.setmAnswer("Topeka");

        //flash card 4
        FlashCard flashcard4 = new FlashCard();
        flashcard4.setmQuestion("What is the capitol of Michigan?");
        flashcard4.setmAnswer("Lansing");

        //flash card 5
        FlashCard flashcard5 = new FlashCard();
        flashcard5.setmQuestion("What is the capitol of Alaska?");
        flashcard5.setmAnswer("Juneau");

        //flash card 6
        FlashCard flashcard6 = new FlashCard();
        flashcard6.setmQuestion("What is the capitol of Utah?");
        flashcard6.setmAnswer("Salt Lake City");

        //flash card 7
        FlashCard flashcard7 = new FlashCard();
        flashcard7.setmQuestion("What is the capitol of Georgia?");
        flashcard7.setmAnswer("Atlanta");

        //flash card 8
        FlashCard flashcard8 = new FlashCard();
        flashcard8.setmQuestion("What is the capitol of Florida?");
        flashcard8.setmAnswer("Tallahassee");

        //flash card 9
        FlashCard flashcard9 = new FlashCard();
        flashcard9.setmQuestion("What is the capitol of Alabama?");
        flashcard9.setmAnswer("Montgomery");

        //flash card 10
        FlashCard flashcard10 = new FlashCard();
        flashcard10.setmQuestion("What is the capitol of Kentucky?");
        flashcard10.setmAnswer("Frankfort");

        cardArrayList.add(flashcard1);
        cardArrayList.add(flashcard2);
        cardArrayList.add(flashcard3);
        cardArrayList.add(flashcard4);
        cardArrayList.add(flashcard5);
        cardArrayList.add(flashcard6);
        cardArrayList.add(flashcard7);
        cardArrayList.add(flashcard8);
        cardArrayList.add(flashcard9);
        cardArrayList.add(flashcard10);
    }

    //return the instance array list of cards.
    public ArrayList<FlashCard> getCardArrayList()
    {
        return cardArrayList;
    }
}
