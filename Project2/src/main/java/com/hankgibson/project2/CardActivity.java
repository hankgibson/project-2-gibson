package com.hankgibson.project2;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CardActivity extends Activity
{
    //create buttons and text views
    private TextView mQuestionEditText;
    private TextView mAnswerEditText;
    public static final String QUESTION_INDEX = "question";
    public static final String ANSWER_INDEX = "answer";
    public static final String ID_INDEX = "id";

    Intent intentFromParent;
    Bundle extras;

    public String question = "What is an Intent?";
    public String answer = "An intent is the glue between activities.";
    public int cardID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        //assign buttons using xml file
        Button mAnswerEditButton = (Button) findViewById(R.id.answer_edit);
        Button mQuestionEditButton = (Button) findViewById(R.id.question_edit);

        //grab the intent and extras
        intentFromParent = getIntent();
        extras = intentFromParent.getExtras();

        //get question and answer from extras
        question = extras.getString(QUESTION_INDEX);
        answer = extras.getString(ANSWER_INDEX);
        cardID = extras.getInt(ID_INDEX);

        //get reference to text views on parent
        mQuestionEditText = (TextView) findViewById(R.id.question_edit_text);
        mAnswerEditText = (TextView) findViewById(R.id.answer_edit_text);

        //set text to string passed in from parent activity
        mQuestionEditText.setText(question);
        mAnswerEditText.setText(answer);

        if (savedInstanceState != null)
        {
            question = savedInstanceState.getString(QUESTION_INDEX);
            answer = savedInstanceState.getString(ANSWER_INDEX);

            //get reference to text views on parent
            mQuestionEditText = (TextView) findViewById(R.id.question_edit_text);
            mAnswerEditText = (TextView) findViewById(R.id.answer_edit_text);

            //set the text views to the strings passed back from saved instance
            mQuestionEditText.setText(question);
            mAnswerEditText.setText(answer);
        }

        mQuestionEditButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent questionEditIntent = new Intent(CardActivity.this, EditActivity.class);

                //pack up intent
                questionEditIntent.putExtra(EditActivity.EDIT, question);
                questionEditIntent.putExtra(EditActivity.SAVED, answer);

                startActivityForResult(questionEditIntent, 0);
            }
        });

        mAnswerEditButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent answerEditIntent = new Intent(CardActivity.this, EditActivity.class);

                //pack up intent
                answerEditIntent.putExtra(EditActivity.EDIT, answer);
                answerEditIntent.putExtra(EditActivity.SAVED, question);

                startActivityForResult(answerEditIntent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //make sure I was returned something
        if (data == null)
        {
            return;
        }

        Bundle extra = data.getExtras();

        //the user wanted to edit the question
        if (requestCode == 0)
        {
            question = extra.getString(EditActivity.EDIT);
            answer = extra.getString(EditActivity.SAVED);

            //get reference to text views on parent
            mQuestionEditText = (TextView) findViewById(R.id.question_edit_text);
            mAnswerEditText = (TextView) findViewById(R.id.answer_edit_text);

            //set the text views to the strings passed back from child
            mQuestionEditText.setText(question);
            mAnswerEditText.setText(answer);
        }

        //the user wanted to edit the answer
        if (requestCode == 1)
        {
            question = extra.getString(EditActivity.SAVED);
            answer = extra.getString(EditActivity.EDIT);

            //get reference to text views on parent
            mQuestionEditText = (TextView) findViewById(R.id.question_edit_text);
            mAnswerEditText = (TextView) findViewById(R.id.answer_edit_text);

            //set the text views to the strings passed back from child
            mQuestionEditText.setText(question);
            mAnswerEditText.setText(answer);
        }
    }

    @Override
    public void onBackPressed()
    {
        intentFromParent.putExtra(QUESTION_INDEX, question);
        intentFromParent.putExtra(ANSWER_INDEX, answer);
        intentFromParent.putExtra(ID_INDEX, cardID);

        setResult(RESULT_OK, intentFromParent);
        finish();

        super.onBackPressed();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);

        //saves data for rotation
        savedInstanceState.putString(QUESTION_INDEX, question);
        savedInstanceState.putString(ANSWER_INDEX, answer);
    }
}