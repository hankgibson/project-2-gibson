package com.hankgibson.project2;

public class FlashCard
{
    private String mQuestion;
    private String mAnswer;

    public FlashCard()
    {
        this.mQuestion = "New question";
        this.mAnswer   = "New answer";
    }

    /*
     * Get card question
     */
    public String getmQuestion()
    {
        return mQuestion;
    }

    /*
     * Set card question
     */
    public void setmQuestion(String mQuestion)
    {
        this.mQuestion = mQuestion;
    }

    /*
     * Get card answer
     */
    public String getmAnswer()
    {
        return mAnswer;
    }

    /*
     * Set card answer
     */
    public void setmAnswer(String mAnswer)
    {
        this.mAnswer = mAnswer;
    }

    @Override
    public String toString()
    {
        return this.mQuestion;
    }
}
