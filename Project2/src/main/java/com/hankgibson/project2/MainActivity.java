package com.hankgibson.project2;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;

public class MainActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getFragmentManager();

        FlashCardFragList fragment = (FlashCardFragList)fragmentManager.findFragmentById(R.id.fragmentContainer);

        //create only if the fragment manager is null else we use old frag.
        if(fragment == null)
        {
            fragment = new FlashCardFragList();

            fragmentManager.beginTransaction().add(R.id.fragmentContainer, fragment ).commit();
        }
    }
}
