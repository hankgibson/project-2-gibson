package com.hankgibson.project2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by hankgibson on 9/12/13.
 */
public class EditActivity extends Activity
{
    public static final String SAVED = "SAVED";
    public static final String EDIT = "EDIT";

    //create buttons
    private Button mCancelButton;
    private Button mSaveButton;
    private EditText mSomethingToEdit;
    private String saved;
    private String edit;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        //assign buttons from xml
        mCancelButton = (Button)findViewById(R.id.cancel_edit);
        mSaveButton = (Button)findViewById(R.id.save_edit);

        //grab the intent and extras
        Intent intentFromParent = getIntent();
        Bundle extras = intentFromParent.getExtras();

        //get question and answer from extras
        saved = extras.getString(SAVED);
        edit = extras.getString(EDIT);

        //what we are editing
        mSomethingToEdit = (EditText)findViewById(R.id.something_to_edit);
        mSomethingToEdit.setText(edit);


        //user cancelled
        mCancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent cancelIntent = new Intent(EditActivity.this, CardActivity.class);

                //nothing to pack
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        //user saved
        mSaveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent saveIntent = new Intent(EditActivity.this, CardActivity.class);

                //pack intent
                saveIntent.putExtra(EDIT, mSomethingToEdit.getText().toString());
                saveIntent.putExtra(SAVED, saved);

                setResult(RESULT_OK, saveIntent);
                finish();
            }
        });
    }
}