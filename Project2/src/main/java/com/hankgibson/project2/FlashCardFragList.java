package com.hankgibson.project2;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

/**
 * Created by hankgibson on 10/3/13.
 */
public class FlashCardFragList extends ListFragment
{
    public static final String LIST_INDEX = "list";
    private ArrayList<FlashCard> cardArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (savedInstanceState == null)
        {
            CardListSingleton.getInstance();
            cardArrayList = CardListSingleton.getInstance().getCardArrayList();
        }

        else
        {
            cardArrayList = CardListSingleton.getInstance().getCardArrayList();
        }

        // Setup adapter
        FlashCardAdapter adapter = new FlashCardAdapter(cardArrayList);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick( ListView l, View v, int position, long id)
    {
        //Create card based on card that is 'clicked'
        FlashCard currentCard = ((FlashCardAdapter)getListAdapter()).getItem(position);
        Intent cardintent = new Intent(getActivity(), CardActivity.class);
        cardintent.putExtra(CardActivity.QUESTION_INDEX, currentCard.getmQuestion());
        cardintent.putExtra(CardActivity.ANSWER_INDEX, currentCard.getmAnswer());
        cardintent.putExtra(CardActivity.ID_INDEX, cardArrayList.indexOf(currentCard));

        //start activity
        startActivityForResult(cardintent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        //create a bundle from the intent to create or edit card
        Bundle extra = data.getExtras();
        FlashCard currentCard = cardArrayList.get(extra.getInt(CardActivity.ID_INDEX));
        currentCard.setmQuestion(extra.getString(CardActivity.QUESTION_INDEX));
        currentCard.setmAnswer(extra.getString(CardActivity.ANSWER_INDEX));

    }

    private class FlashCardAdapter extends ArrayAdapter<FlashCard>
    {
        public FlashCardAdapter(ArrayList<FlashCard> cards)
        {
            super(getActivity(), android.R.layout.simple_list_item_1, cards);
        }
    }

    //creates the options menu in order to create a new card
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    //listener for a click to create a new card
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.action_addcard:
                FlashCard newCard = new FlashCard();
                cardArrayList.add(0, newCard);
                Intent cardintent = new Intent(getActivity(), CardActivity.class);
                cardintent.putExtra(CardActivity.QUESTION_INDEX, newCard.getmQuestion());
                cardintent.putExtra(CardActivity.ANSWER_INDEX, newCard.getmAnswer());
                cardintent.putExtra(CardActivity.ID_INDEX, cardArrayList.indexOf(newCard));
                startActivityForResult(cardintent, 1);
        }
        return true;
    }

    //sets the correct list up
    @Override
    public void onResume()
    {
        super.onResume();
        ((ArrayAdapter)getListAdapter()).notifyDataSetChanged();
    }

    //saved instane state to allow the user to destroy thier activity and have it come back to the good state with erasing everything.
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);

       //saves data for rotation
       savedInstanceState.putInt(LIST_INDEX, 1);
    }
}